<?php

namespace Database\Seeders;

use App\Models\Group;
use App\Models\Image;
use App\Models\Order;
use App\Models\Parameter;
use App\Models\Product;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TestDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Group::factory(3)->create();



        Order::factory(8)->has(
            Product::factory(16)->has(
                Parameter::factory(5),
            )->has(
                Image::factory(5)
            )
        )->create();

    }
}
