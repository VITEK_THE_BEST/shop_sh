<?php

namespace App\Http\Controllers;

use App\Models\Image;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


/**
 * @authenticated
 * @group изображения
 */
class ImageController extends Controller
{
    public function create(Request $request,Product $product)
    {
        $validate = $request->validate([
            'image' => 'required',
        ]);

        $validate['product_id'] = $product->id;

        $image_parts = explode(";base64,", $validate['image']);
        $image_base64 = base64_decode($image_parts[1]);
        $file = 'images/' . uniqid() . '.jpg';

        Storage::disk('local')->put($file, $image_base64);

        $validate['image'] = $file;

        $parameter = Image::query()->create($validate);

        return response()->json($parameter);
    }

    /**
     * Получить изображения по id продукта
     * */
    public function show(Product $product)
    {
        $product = $product->images()->get()->map(function ($image){
            $imageData = Storage::disk('local')->get($image['image']);
            $image['image'] =  "data:image/png;base64, ".base64_encode($imageData);
            return $image;
        });
        return response()->json($product);
    }


//    public function update(Request $request, Image $image)
//    {
//        $validate = $request->validate([
//            'image' => 'required',
//        ]);
//
//        $image->update($validate);
//        return response()->json([]);
//    }

    public function delete(Image $image)
    {
        $image->delete();
        return response()->json([]);
    }
}
