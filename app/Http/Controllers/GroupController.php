<?php

namespace App\Http\Controllers;

use App\Models\Group;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

/**
 * @authenticated
 * @group группы
 */
class GroupController extends Controller
{
    public function create(Request $request)
    {
        $validate = $request->validate([
            'name' => 'required|string',
        ]);

        $group = Group::query()->create($validate);

        return response()->json($group);
    }

    /**
     * отобразить все группы со  всеми продуктами и изображениями
     */
    public function show()
    {
        $groups = Group::query()
            ->with([
                'products.images'
            ])
            ->get();

        $groups = $groups->map(function ($group) {
            $group['products']->map(function ($product) {
                $product['images']->map(function ($image) {
                    $imageData = Storage::disk('local')->get($image['image']);
                    $image['image'] = "data:image/png;base64, ".base64_encode($imageData);
                    return $image;
                });
                return $product;
            });
            return $group;
        });
        return response()->json($groups);
    }

    /**
     * отобразить продукты группы с параметрами
     */
    public function showDetail(Group $group)
    {
        $group = Group::query()
            ->with([
                'products.parameters',
                'products.images'
            ])->find($group->id);

        $group['products']->map(function ($product) {
            $product['images']->map(function ($image) {
                $imageData = Storage::disk('local')->get($image['image']);
                $image['image'] = "data:image/png;base64, ".base64_encode($imageData);
                return $image;
            });
            return $product;
        });
        return response()->json($group);
    }

    public function update(Request $request, Group $group)
    {
        $validate = $request->validate([
            'name' => 'required|string',
        ]);

        $group->update($validate);
        return response()->json([]);
    }

    public function delete(Group $group)
    {
        $group->delete();
        return response()->json([]);
    }
}
