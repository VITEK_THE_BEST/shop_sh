<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class GroupProduct
 * 
 * @property int $id
 * @property int $group_id
 * @property int $product_id
 * 
 * @property Group $group
 * @property Product $product
 *
 * @package App\Models
 */
class GroupProduct extends Model
{
	protected $table = 'group_product';
	public $timestamps = false;

	protected $casts = [
		'group_id' => 'int',
		'product_id' => 'int'
	];

	protected $fillable = [
		'group_id',
		'product_id'
	];

	public function group()
	{
		return $this->belongsTo(Group::class);
	}

	public function product()
	{
		return $this->belongsTo(Product::class);
	}
}
