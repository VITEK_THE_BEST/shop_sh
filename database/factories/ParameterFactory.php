<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Parameter>
 */
class ParameterFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $size = ['X','L','M'];

        return [
            'product_id' => Product::query()->inRandomOrder()->first()->value('id'),
            'size' => $size[array_rand($size)],
            'in_stock' => true,
        ];
    }
}
