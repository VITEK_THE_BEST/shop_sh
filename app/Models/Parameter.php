<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Parameter
 *
 * @property int $id
 * @property int $product_id
 * @property string $size
 * @property bool $in_stock
 *
 * @property Product $product
 *
 * @package App\Models
 */
class Parameter extends Model
{
    use HasFactory;

    protected $table = 'parameters';
	public $timestamps = false;

	protected $casts = [
		'product_id' => 'int',
		'in_stock' => 'bool'
	];

	protected $fillable = [
		'product_id',
		'size',
		'in_stock'
	];

	public function product()
	{
		return $this->belongsTo(Product::class);
	}
}
