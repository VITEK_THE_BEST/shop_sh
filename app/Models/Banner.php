<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Banner
 * 
 * @property int $id
 * @property string $image
 *
 * @package App\Models
 */
class Banner extends Model
{
	protected $table = 'banners';
	public $timestamps = false;

	protected $fillable = [
		'image'
	];
}
