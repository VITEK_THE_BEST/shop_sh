<?php

namespace App\Http\Controllers;

use App\Models\Group;
use App\Models\Image;
use App\Models\Parameter;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

/**
 * @authenticated
 * @group продукты
 */
class ProductController extends Controller
{
    /**
     * создание продукта вместе с параметром и изображением
     */
    public function create(Request $request, Group $group)
    {
        $validate = $request->validate([
            'name' => 'required|string',
            'price' => 'required',
            'article' => 'required|string|unique:products',
            'size' => 'required|string',
            'in_stock' => 'required|boolean',
            'image' => 'required',
        ]);
        $validate['group_id'] = $group->id;
        $product = Product::query()->create($validate);

        $parameter = Parameter::query()->create([
            'product_id' => $product->id,
            'size' => $validate['size'],
            'in_stock' => $validate['in_stock'],
        ]);
        $product['parameter'] = $parameter;

        $validate['product_id'] = $product->id;

        $image_parts = explode(";base64,", $validate['image']);
        $image_base64 = base64_decode($image_parts[1]);
        $file = 'images/' . uniqid() . '.jpg';

        Storage::disk('local')->put($file, $image_base64);

        $validate['image'] = $file;

        Image::query()->create($validate);

        return response()->json($product);
    }

    /**
     * добавить групппу в продукт
     */
    public function attaching(Product $product, Group $group)
    {
        $group->products()->save($product);
        return response()->json([]);
    }

    /**
     * удалить группу из продукта
     */
    public function detaching(Product $product, Group $group)
    {
        $group->products()->delete($product);
        return response()->json([]);
    }

    /**
     * отобразить  продукты группы по продукту
     */
    public function showGroupByProduct(Product $product)
    {
        $group_products = $product
            ->group()
            ->with('products.images')
            ->first();

        $products = $group_products['products']
            ->filter(function ($product_it) use ($product) {
                return $product_it['id'] != $product['id'];
            });

        $group_products = $group_products->toArray();
        $products = $products->map(function ($product) {
            $product['images']->map(function ($image) {
                $imageData = Storage::disk('local')->get($image['image']);
                $image['image'] = "data:image/png;base64, ".base64_encode($imageData);
                return $image;
            });
            return $product;
        });
        $group_products['products'] = $products->values()->toArray();

        return response()->json($group_products);
    }

    /**
     * детальная информация о продукте
     *
     * выведет изображения и параметры продукта
     */
    public function showDetail(Product $product)
    {
        $product = Product::query()->with([
            "images",
            "parameters",
        ])->find($product->id);

        $product['images']->map(function ($image) {
            $imageData = Storage::disk('local')->get($image['image']);
            $image['image'] = "data:image/png;base64, ".base64_encode($imageData);
            return $image;
        });

        return response()->json($product);
    }

    /**
     * обновить данные продукта
     *
     * принимает продукт, имя,цену, артикуль, массив параметров и массив изображений
    */
    public function update(Request $request, Product $product)
    {
        $validate = $request->validate([
            'name' => 'required|string',
            'price' => 'required',
            'article' => 'required|string',
            'parameters' => 'sometimes', #array
            'images' => 'sometimes',#array
        ]);

        foreach ($validate['parameters'] as $parameter){
            $param = Parameter::query()->findOrFail($parameter['id']);
            $param->update($parameter);
        }

        foreach ($validate['images'] as $image){
            $img = Image::query()->find($image['id']);
            $image_parts = explode(";base64,", $image['image']);
            $image_base64 = base64_decode($image_parts[1]);
            Storage::disk('local')->put($img['image'], $image_base64);
        }

        $product->update($validate);
        return response()->json([]);
    }

    public function delete(Product $product)
    {
        $product->delete();
        return response()->json([]);
    }
}
