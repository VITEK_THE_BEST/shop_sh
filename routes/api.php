<?php

use App\Http\Controllers\BannerController;
use App\Http\Controllers\GroupController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ParameterController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ImageController;
use Illuminate\Support\Facades\Route;

/**
 * @unauthenticated
 */
Route::post('/registration', [UserController::class, 'register']);

/**
 * @unauthenticated
 */
Route::post('/getToken', [UserController::class, 'getToken']);

Route::middleware(['api', 'auth:sanctum'])->group(function () {

    Route::group(['prefix' => 'user'], function () {
        Route::get('/me', [UserController::class, 'me']);
        Route::delete('/dropToken', [UserController::class, 'dropToken']);
        Route::patch('/update', [UserController::class, 'update']);
        Route::delete('/delete/{id}', [UserController::class, 'delete']);
    });

    Route::group(['prefix' => 'group'], function () {
        Route::post('/create', [GroupController::class, 'create']);
        Route::get('/showDetail/{group}', [GroupController::class, 'showDetail']);
        Route::patch('/update/{group}', [GroupController::class, 'update']);
        Route::delete('/delete/{group}', [GroupController::class, 'delete']);
    });

    Route::group(['prefix' => 'product'], function () {
        Route::post('/create/{group}', [ProductController::class, 'create']);
        Route::get('/{product}/attaching/{group}', [ProductController::class, 'attaching']);
        Route::get('/{product}/detaching/{group}', [ProductController::class, 'detaching']);
        Route::patch('/update/{product}', [ProductController::class, 'update']);
        Route::delete('/delete/{product}', [ProductController::class, 'delete']);
    });

    Route::group(['prefix' => 'parameter'], function () {
        Route::post('/create/{product}', [ParameterController::class, 'create']);
        Route::patch('/update/{parameter}', [ParameterController::class, 'update']);
        Route::delete('/delete/{parameter}', [ParameterController::class, 'delete']);
    });

    Route::group(['prefix' => 'image'], function () {
        Route::post('/create/{product}', [ImageController::class, 'create']);
        Route::patch('/update/{image}', [ImageController::class, 'update']);
        Route::delete('/delete/{image}', [ImageController::class, 'delete']);
    });

    Route::group(['prefix' => 'banner'], function () {
        Route::post('/create', [BannerController::class, 'create']);
        Route::patch('/update/{banner}', [BannerController::class, 'update']);
        Route::delete('/delete/{banner}', [BannerController::class, 'delete']);
    });
    Route::group(['prefix' => 'order'], function () {
        Route::get('/show', [OrderController::class, 'show']);
        Route::patch('/update/{order}', [OrderController::class, 'update']);
        Route::delete('/delete/{order}', [OrderController::class, 'delete']);
    });
});

Route::group(['prefix' => 'order'], function () {
    Route::post('/create', [OrderController::class, 'create']);
});

Route::group(['prefix' => 'group'], function () {
    Route::get('/show', [GroupController::class, 'show']);
});

Route::group(['prefix' => 'product'], function () {
    Route::get('/showDetail/{product}', [ProductController::class, 'showDetail']);
    Route::get('/showGroupByProduct/{product}', [ProductController::class, 'showGroupByProduct']);
});

Route::group(['prefix' => 'parameter'], function () {
    Route::get('/show/{product}', [ParameterController::class, 'show']);
});

Route::group(['prefix' => 'image'], function () {
    Route::get('/show/{product}', [ImageController::class, 'show']);
});

Route::group(['prefix' => 'banner'], function () {
    Route::get('/show', [BannerController::class, 'show']);
});
