<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

/**
 * @authenticated
 * @group банера
 */
class BannerController extends Controller
{
    public function create(Request $request)
    {
        $validate = $request->validate([
            'image' => 'required',
        ]);

        $image_parts = explode(";base64,", $validate['image']);
        $image_base64 = base64_decode($image_parts[1]);
        $file = 'images/' . uniqid() . '.jpg';

        Storage::disk('local')->put($file, $image_base64);

        $validate['image'] = $file;
        $banner = Banner::query()->create($validate);

        return response()->json($banner);
    }

    /**
     * Получить все банеры
     * */
    public function show()
    {
        $banners = Banner::all();
        $banners = $banners->map(function ($banner){
            $imagedata = Storage::disk('local')->get($banner['image']);
            $banner['image'] =  "data:image/png;base64, ".base64_encode($imagedata);
            return $banner;
        });
        return response()->json($banners);
    }


//    public function update(Request $request, Banner $banner)
//    {
//        $validate = $request->validate([
//            'image' => 'required',
//        ]);
//
//        $banner->update($validate);
//        return response()->json([]);
//    }

    public function delete(Banner $banner)
    {
        $banner->delete();
        return response()->json([]);
    }
}
