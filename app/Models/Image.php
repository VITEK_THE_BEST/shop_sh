<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Image
 *
 * @property int $id
 * @property int $product_id
 * @property string $image
 *
 * @property Product $product
 *
 * @package App\Models
 */
class Image extends Model
{
    use HasFactory;

    protected $table = 'images';
    public $timestamps = false;

    protected $casts = [
        'product_id' => 'int'
    ];

    protected $fillable = [
        'product_id',
        'image'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
