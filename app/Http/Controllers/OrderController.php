<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Создать заказ
     *
    */
    public function create(Request $request)
    {
        $validate = $request->validate([
            'fio' => 'required|string',
            'phone' => 'required',
            'address' => 'required',
            'products' => 'required'
        ]);
        $validate['status'] = 'В обработке';

        $order = Order::query()->create($validate);

        foreach ($validate['products'] as $product){
            for ($i = 0; $i < $product['count'];$i++){
                $order->products()->attach($product['id']);
            }
        }

        return response()->json([]);
    }

    /**
     * получить все заказы
     * */
    public function show()
    {
        $orders = Order::query()
            ->with('products')
            ->get();

        $prices = $orders->pluck('products.*.price');
        $orders = $orders->toArray();

        foreach ($orders as $key=>$order){
            $orders[$key]['total'] = array_sum($prices[$key]);
        }


        return response()->json($orders);
    }

    /**
     * обновить данные заказы.
     *
     *
     * что отправишь то и обновится
     * */
    public function update(Request $request, Order $order)
    {
        $validate = $request->validate([
            'fio' => 'sometimes|string',
            'phone' => 'sometimes',
            'address' => 'sometimes',
            'status' => 'sometimes',
        ]);

        $order->update($validate);
        return response()->json([]);
    }

    /**
     * удалить заказ
     * */
    public function delete(Order $order)
    {
        $order->delete();
        return response()->json([]);
    }
}
