<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::query()->create([
            "first_name" => "debitis",
            "last_name" => "blanditiis",
            "email" => "test@mail.ru",
            'password' => "$2y$10$.WdNTuj8ekwXph55PMK4beqlCD/vyH8MU4QzaxvyTrkhjGp3bJOCe"
        ]);
    }
}
