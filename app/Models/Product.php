<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Product
 *
 * @property int $id
 * @property int $group_id
 * @property string $name
 * @property string $article
 * @property float $price
 *
 * @property Group $group
 * @property Collection|Image[] $images
 * @property Collection|Parameter[] $parameters
 *
 * @package App\Models
 */
class Product extends Model
{
    use HasFactory;

    protected $table = 'products';
	public $timestamps = false;

	protected $casts = [
		'group_id' => 'int',
		'price' => 'float'
	];

	protected $fillable = [
		'group_id',
		'name',
		'article',
		'price'
	];

	public function group()
	{
		return $this->belongsTo(Group::class);
	}

	public function images()
	{
		return $this->hasMany(Image::class);
	}

	public function parameters()
	{
		return $this->hasMany(Parameter::class);
	}
    public function orders()
    {
        return $this->belongsToMany(Order::class);
    }
}
