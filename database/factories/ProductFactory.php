<?php

namespace Database\Factories;

use App\Models\Group;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'group_id'=>Group::query()->inRandomOrder()->first()->value('id'),
            'name'=>$this->faker->name(),
            'price'=>$this->faker->numberBetween(50,9000),
            'article'=>$this->faker->unique()->streetSuffix() ,
        ];
    }
}
