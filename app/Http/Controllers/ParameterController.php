<?php

namespace App\Http\Controllers;

use App\Models\Parameter;
use App\Models\Product;
use Illuminate\Http\Request;

/**
 * @authenticated
 * @group параметры
 */
class ParameterController extends Controller
{
    public function create(Request $request,Product $product)
    {
        $validate = $request->validate([
            'size' => 'required|string',
            'in_stock' => 'sometimes|boolean',
        ]);

        $validate['product_id'] = $product->id;

        $parameter = Parameter::query()->create($validate);

        return response()->json($parameter);
    }

    /**
     * Получить параметры по id продукта
     * */
    public function show(Product $product)
    {
        return response()->json($product->parameter()->get());
    }


    public function update(Request $request, Parameter $parameter)
    {
        $validate = $request->validate([
            'size' => 'sometimes|string',
            'in_stock' => 'sometimes|boolean',
        ]);

        $parameter->update($validate);
        return response()->json([]);
    }

    public function delete(Parameter $parameter)
    {
        $parameter->delete();
        return response()->json([]);
    }
}
